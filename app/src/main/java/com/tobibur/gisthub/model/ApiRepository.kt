package com.tobibur.gisthub.model

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import com.tobibur.gisthub.model.repos.Repos
import com.tobibur.gisthub.model.user.User
import com.tobibur.gisthub.network.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import javax.inject.Inject

class ApiRepository @Inject constructor(retrofit: Retrofit) {

    val apiResponse = MutableLiveData<ApiResponse>()
    private val apiService = retrofit.create(ApiInterface::class.java)

    fun getRepoService(user: String): LiveData<ApiResponse> {
        val call : Call<List<Repos>> = apiService.getRepos(user)
        call.enqueue(object : Callback<List<Repos>> {
            override fun onFailure(call: Call<List<Repos>>?, t: Throwable?) {
                apiResponse.postValue(ApiResponse(t!!))
            }

            override fun onResponse(call: Call<List<Repos>>?, response: Response<List<Repos>>?) {
                if (response!!.isSuccessful) {
                    apiResponse.postValue(ApiResponse(response.body()!!))
                }else{
                    apiResponse.postValue(ApiResponse(response.code()))
                }
            }

        })

        return apiResponse
    }

    fun getUser(user: String): LiveData<ApiResponse> {
        val call : Call<User> = apiService.getUserProfile(user)
        call.enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>?, t: Throwable?) {
                apiResponse.postValue(ApiResponse(t!!))
            }

            override fun onResponse(call: Call<User>?, response: Response<User>?) {
                if (response!!.isSuccessful) {
                    apiResponse.postValue(ApiResponse(response.body()!!))
                }else{
                    apiResponse.postValue(ApiResponse(response.code()))
                }
            }

        })

        return apiResponse
    }
}