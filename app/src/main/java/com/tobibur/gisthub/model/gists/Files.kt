package com.tobibur.gisthub.model.gists

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Files {

    @SerializedName("filename")
    @Expose
    var fileName: String? = null
    var raw_url: String? = null
    @SerializedName("size")
    var fileSize: String? = null
}