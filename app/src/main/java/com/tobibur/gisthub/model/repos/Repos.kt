package com.tobibur.gisthub.model.repos

import com.google.gson.annotations.SerializedName

class Repos {

    @SerializedName("name")
    var repo_name : String? = null

    @SerializedName("description")
    var desc : String? = null

    @SerializedName("fork")
    var isFork : Boolean = false

    @SerializedName("language")
    var lang : String? = null

    @SerializedName("stargazers_count")
    var stars : String? = null

    @SerializedName("forks_count")
    var forks: String? = null


}