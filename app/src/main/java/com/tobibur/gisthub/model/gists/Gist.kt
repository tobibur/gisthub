package com.tobibur.gisthub.model.gists

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Gist {
    @SerializedName("files")
    @Expose
    var files: Files? = null

    @SerializedName("commits_url")
    @Expose
    var commits_url: String? = null

    @SerializedName("truncated")
    @Expose
    var truncated: String? = null

    @SerializedName("git_push_url")
    @Expose
    var git_push_url: String? = null

    @SerializedName("url")
    @Expose
    var url: String? = null

    @SerializedName("node_id")
    @Expose
    var node_id: String? = null

    @SerializedName("id")
    @Expose
    var id: String? = null

    @SerializedName("html_url")
    @Expose
    var html_url: String? = null

    @SerializedName("updated_at")
    @Expose
    var updated_at: String? = null

    @SerializedName("comments_url")
    @Expose
    var comments_url: String? = null

    @SerializedName("description")
    @Expose
    var description: String? = null

    @SerializedName("owner")
    @Expose
    var owner: OwnerInfo? = null

    @SerializedName("forks_url")
    @Expose
    var forks_url: String? = null

    @SerializedName("created_at")
    @Expose
    var created_at: String? = null

    @SerializedName("comments")
    @Expose
    var comments: String? = null

    @SerializedName("git_pull_url")
    @Expose
    var git_pull_url: String? = null

    override fun toString(): String {
        return "ClassPojo [files = $files, commits_url = $commits_url, truncated = $truncated, git_push_url = $git_push_url, url = $url, node_id = $node_id, id = $id, html_url = $html_url, updated_at = $updated_at, comments_url = $comments_url, description = $description, owner = $owner, forks_url = $forks_url, created_at = $created_at, comments = $comments, git_pull_url = $git_pull_url]"
    }
}
