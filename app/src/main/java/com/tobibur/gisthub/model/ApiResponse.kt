package com.tobibur.gisthub.model

import com.tobibur.gisthub.model.repos.Repos
import com.tobibur.gisthub.model.user.User

class ApiResponse {

    var posts: List<Repos>? = null
    lateinit var user: User
    var error: Throwable? = null
    var code : Int? = null

    constructor(posts: List<Repos>) {
        this.posts = posts
        this.error = null
    }

    constructor(posts: User) {
        this.user = posts
        this.error = null
    }

    constructor(error: Throwable) {
        this.error = error
        this.posts = null
    }

    constructor(code : Int){
        this.code = code
    }
}