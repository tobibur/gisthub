package com.tobibur.gisthub.network

import com.tobibur.gisthub.model.gists.Gist
import com.tobibur.gisthub.model.repos.Repos
import com.tobibur.gisthub.model.user.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {

    @GET("users/{username}")
    fun getUserProfile(@Path("username") username : String) : Call<User>

    @GET("users/{username}/repos")
    fun getRepos(@Path("username") username: String) : Call<List<Repos>>
}