package com.tobibur.gisthub.di.modules

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.Retrofit
import javax.inject.Singleton



@Module
class AndroidModule(private val application: Application){

    private val BASE_URL = "https://api.github.com/"

    @Provides
    fun provideApplicationContext() : Context = application

    @Provides
    @Singleton
    fun provideRetrofit(): Retrofit {

        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
    }
}