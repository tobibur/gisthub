package com.tobibur.gisthub.di

import com.tobibur.gisthub.view.MainActivity
import com.tobibur.gisthub.di.modules.AndroidModule
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [(AndroidModule::class)])
interface ApplicationComponent{

    fun inject(activity : MainActivity)
}