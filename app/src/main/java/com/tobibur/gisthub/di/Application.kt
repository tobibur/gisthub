package com.tobibur.gisthub.di

import android.app.Application
import com.tobibur.gisthub.di.ApplicationComponent
import com.tobibur.gisthub.di.modules.AndroidModule

class Application : Application() {

    lateinit var component: ApplicationComponent

    override fun onCreate() {
        super.onCreate()

        component = DaggerApplicationComponent.builder()
                .androidModule(AndroidModule(this))
                .build()
    }
}