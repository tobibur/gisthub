package com.tobibur.gisthub.di

import android.support.v7.app.AppCompatActivity
import com.tobibur.gisthub.di.Application
import com.tobibur.gisthub.di.ApplicationComponent

//fun AppCompatActivity.component() : ApplicationComponent =  (application as Application).component

val AppCompatActivity.component : ApplicationComponent
        get() =  (application as Application).component