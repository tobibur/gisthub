package com.tobibur.gisthub.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import com.tobibur.gisthub.model.ApiRepository
import com.tobibur.gisthub.model.ApiResponse
import retrofit2.Retrofit

class MainActivityViewModel : ViewModel(){

    private var quote : LiveData<ApiResponse>? = null
    private var quote1 : LiveData<ApiResponse>? = null
    private lateinit var mApiRepo: ApiRepository

    fun getQuoteData(retrofit: Retrofit,username: String, refresh : Boolean = false): LiveData<ApiResponse>{
        if(refresh){
            quote = null
        }
        if (this.quote == null) {
            mApiRepo = ApiRepository(retrofit)
            quote = mApiRepo.getRepoService(username)
            return quote as LiveData<ApiResponse>
        }
        return quote as LiveData<ApiResponse>
    }

    fun getProfile(retrofit: Retrofit,username: String, refresh : Boolean = false): LiveData<ApiResponse>{
        if(refresh){
            quote1 = null
        }
        if (this.quote1 == null) {
            mApiRepo = ApiRepository(retrofit)
            quote1 = mApiRepo.getUser(username)
            return quote1 as LiveData<ApiResponse>
        }
        return quote1 as LiveData<ApiResponse>
    }
}