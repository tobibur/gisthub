package com.tobibur.gisthub.view.adapter;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tobibur.gisthub.R;
import com.tobibur.gisthub.model.repos.Repos;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<ViewHolder>{

    private List<Repos> mRepos;
    private View.OnClickListener mOnClickListener;

    public RecyclerAdapter(List<Repos> repos, View.OnClickListener onClickListener) {
        this.mRepos = repos;
        this.mOnClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.repo_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Repos repos = mRepos.get(i);
        viewHolder.repoName.setText(repos.getRepo_name());
        viewHolder.repoDesc.setText(repos.getDesc());
        viewHolder.repoLang.setText(repos.getLang());
        viewHolder.repoStar.setText("Stars: "+repos.getStars());
        viewHolder.repoFork.setText("Forks: "+repos.getForks());
        viewHolder.itemView.setTag(repos);
        viewHolder.itemView.setOnClickListener(mOnClickListener);
    }

    @Override
    public int getItemCount() {
        return mRepos.size();
    }
}
