package com.tobibur.gisthub.view

import android.annotation.SuppressLint
import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import com.tobibur.gisthub.R
import com.tobibur.gisthub.model.repos.Repos
import com.tobibur.gisthub.di.component
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Retrofit
import javax.inject.Inject
import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.afollestad.aesthetic.Aesthetic
import com.afollestad.aesthetic.AestheticActivity
import com.bumptech.glide.Glide
import com.miguelcatalan.materialsearchview.MaterialSearchView
import com.tobibur.gisthub.view.adapter.RecyclerAdapter
import com.roger.catloadinglibrary.CatLoadingView
import com.tobibur.gisthub.model.user.User
import com.tobibur.gisthub.viewmodel.MainActivityViewModel
import org.jetbrains.anko.*


class MainActivity : AestheticActivity() , AnkoLogger, View.OnClickListener {


    @Inject
    lateinit var retrofit: Retrofit

    private var adapter: RecyclerAdapter? = null

    var mView: CatLoadingView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(toolbar)

        //Default Theme
        if (Aesthetic.isFirstTime()) {
            defaultTheme()
        }

        component.inject(this)

        mView = CatLoadingView()

        //gistService()


        repoService("Tobibur",false)
        searchClicked()

        mView!!.show(supportFragmentManager, "Loading")
    }

    private fun defaultTheme() {
        Aesthetic.get()
                .activityTheme(R.style.AppTheme)
                .isDark(false)
                .colorPrimaryRes(R.color.colorPrimary)
                .colorStatusBarRes(R.color.colorPrimaryDark)
                .textColorPrimaryRes(R.color.colorTextBlue)
                .textColorPrimaryInverseRes(android.R.color.white)
                .textColorSecondaryRes(R.color.colorTextGrey)
                .textColorSecondaryInverseRes(R.color.colorAccent)
                .colorCardViewBackgroundRes(R.color.colorBackground)
                .apply()
    }

    private fun searchClicked() {
        search_view.setOnQueryTextListener(object : MaterialSearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                //Do some magic
                repoService(query,true)
                //supportActionBar?.title = query

                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                //Do some magic
                return false
            }
        })

        search_view.setOnSearchViewListener(object : MaterialSearchView.SearchViewListener {
            override fun onSearchViewShown() {
                //Do some magic
            }

            override fun onSearchViewClosed() {
                //Do some magic
            }
        })

        search_view.setVoiceSearch(true) //or false
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == MaterialSearchView.REQUEST_VOICE && resultCode == Activity.RESULT_OK) {
            val matches = data!!.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            if (matches != null && matches.size > 0) {
                val searchWrd = matches[0]
                if (!TextUtils.isEmpty(searchWrd)) {
                    search_view.setQuery(searchWrd, false)
                }
            }

            return
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.search_menu, menu)

        val item = menu.findItem(R.id.search_menu_btn)
        search_view.setMenuItem(item)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        var id = item?.itemId
        if (id==R.id.theme) {

            val themes = listOf("Green", "Red", "Dark", "Default")
            selector("Pick a theme", themes) { _, i ->
                toast("Theme ${themes[i]} selected!")

                if (themes[i] == "Dark") {
                    Aesthetic.get()
                            .activityTheme(R.style.Theme_AppCompat_NoActionBar)
                            .isDark(false)
                            .colorPrimaryRes(R.color.colorBackgroundDark)
                            .colorStatusBarRes(R.color.colorBackgroundDark)
                            .textColorPrimaryRes(R.color.colorTeal)
                            .textColorPrimaryInverseRes(android.R.color.white)
                            .textColorSecondaryRes(R.color.colorBackground)
                            .textColorSecondaryInverseRes(R.color.colorBackground)
                            .colorCardViewBackgroundRes(R.color.colorBackgroundDark)
                            .apply()
                }else if(themes[i]== "Default"){
                    defaultTheme()
                }
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun repoService(user: String, refresh: Boolean) {
        val viewModel = ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
        viewModel.getQuoteData(retrofit, user, refresh).observe(this, Observer {
            mView!!.dismiss()
            if(it == null){
                info("Handle Error")
            }
            if(it?.error == null){
                if(it?.code==null) {
                    val repos: List<Repos>? = it!!.posts
                    fillData(repos)

                }else{
                    when(it.code!!){
                        404 -> toast("Sorry User not found! :(")
                        else ->{
                            toast("Error! Please try again..")
                        }
                    }
                }
            }else{
                val e : Throwable = it.error!!
                info("Error is " + e.message)
            }
        })

        viewModel.getProfile(retrofit, user, refresh).observe(this, Observer {
            if(it == null){
                info("Handle Error")
            }
            if(it?.error == null){
                if(it?.code==null) {
                    val profile: User? = it!!.user
                    fillProfile(profile)

                }else{
                    when(it.code!!){
                        404 -> toast("Sorry User not found! :(")
                        else ->{
                            toast("Error! Please try again..")
                        }
                    }
                }
            }else{
                val e : Throwable = it.error!!
                info("Error is " + e.message)
            }
        })

    }

    @SuppressLint("SetTextI18n")
    private fun fillProfile(profile: User?) {
        if (profile != null) {
            handle.text = profile.login
            Username.text = profile.name
            supportActionBar?.title = profile.name
            followers.text = "Followers: " + profile.followers.toString()+" Following: "+profile.following.toString()
            Glide.with(applicationContext).load(profile.avatar_url).into(avatar)
        }

    }

    private fun fillData(repos: List<Repos>?) {
        val linearLayoutManager = LinearLayoutManager(this)
        repoRecyclerView.layoutManager = linearLayoutManager

        repoRecyclerView.setHasFixedSize(true)

        adapter = RecyclerAdapter(repos,this)

        repoRecyclerView.adapter = adapter

    }

    override fun onClick(p0: View?){
        val repos : Repos = p0!!.tag as Repos
        toast(repos.repo_name!!)
    }
}
