package com.tobibur.gisthub.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tobibur.gisthub.R;


public class ViewHolder extends RecyclerView.ViewHolder{

    public TextView repoName, repoDesc, repoLang, repoStar, repoFork;

    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        repoName = itemView.findViewById(R.id.repoNameTV);
        repoDesc = itemView.findViewById(R.id.descTV);
        repoLang = itemView.findViewById(R.id.langTV);
        repoStar =  itemView.findViewById(R.id.starsTV);
        repoFork = itemView.findViewById(R.id.forksTV);
    }
}
